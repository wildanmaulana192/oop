<?php
require_once('animal.php');

class frog extends Animal{
    public $name = "buduk";
    public $legs = 4; 
    public $cold_blooded = "no";
    public $hop_hop = "Hop Hop";
    
    public function _construct($string){
        $this ->name = $string;
    }
}

?>