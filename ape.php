<?php
require_once('animal.php');

class Ape extends Animal{
    public $name = "kera sakti";
    public $legs = 2; 
    public $cold_blooded = "no";
    public $yell = "Auooo";
    
    public function _construct($string){
        $this ->name = $string;
    }
}

?>

